<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
     return view('table');
});

Route::get('/table', function (){
     return view('table');
});

Route::get('/data-tables', function (){
     return view('datatable');
});

Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@Store');
Route::get('/cast', 'CastController@Index');

Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');