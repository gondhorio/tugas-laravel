@extends('adminlte.master')

@section('content')
	<form action="/cast" method="post">
	@csrf
	<div class="card-body">
	
		<div class="form-group">
		<label for="exampleInputEmail1">Nama</label>
		<input type="text" class="form-control" id="Nama" name="Nama" placeholder="" value="{{ old('Nama', '') }}">
		@error('Nama')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror
		</div>
		
		<div class="form-group">
		<label for="exampleInputEmail1">Umur</label>
		<input type="number" class="form-control" id="Umur" name="Umur" placeholder="" value="{{ old('Umur', '') }}">
		@error('Umur')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror
		</div>
		
		<div class="form-group">
		<label for="exampleInputPassword1">Bio</label>
		<input type="text" class="form-control" id="Bio" name="Bio" placeholder="" value="{{ old('Bio', '') }}">
		@error('Bio')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror
		</div>
		
		</div>
		
		<div class="card-footer">
		<button type="submit" class="btn btn-primary">Submit</button>
		<button type="button" class="btn btn-primary" onclick="document.location.href='/cast'">Cancel</button>
	</div>
	</form>
@endsection

@push('scripts')
	
@endpush