@extends('adminlte.master')

@section('content')
	
	<div class="card-body">		
		<div>
			<h2>Edit Cast {{$cast->id}}</h2>
			<form action="/cast/{{$cast->id}}" method="POST">
				@csrf
				@method('PUT')
				<div class="form-group">
					<label for="Nama">Nama</label>
					<input type="text" class="form-control" name="Nama" value="{{$cast->nama}}" id="Nama" placeholder="Masukkan Nama">
					@error('Nama')
						<div class="alert alert-danger">
							{{ $message }}
						</div>
					@enderror
				</div>
				<div class="form-group">
					<label for="Umur">Umur</label>
					<input type="number" class="form-control" name="Umur" value="{{$cast->umur}}" id="Umur" placeholder="Masukkan Umur">
					@error('Umur')
						<div class="alert alert-danger">
							{{ $message }}
						</div>
					@enderror
				</div>
				<div class="form-group">
					<label for="Bio">Bio</label>
					<input type="text" class="form-control" name="Bio"  value="{{$cast->bio}}"  id="Bio" placeholder="Masukkan Bio">
					@error('Bio')
						<div class="alert alert-danger">
							{{ $message }}
						</div>
					@enderror
				</div>
				<button type="submit" class="btn btn-primary">Edit</button>
				<button type="button" class="btn btn-primary" onclick="document.location.href='/cast'">Cancel</button>
			</form>
		</div>
	</div>
	
@endsection

@push('scripts')
	
@endpush