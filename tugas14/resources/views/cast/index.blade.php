@extends('adminlte.master')

@section('content')
	
	<div class="card-body">
		<a href="/cast/create" class="btn btn-primary">Tambah</a>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="width: 10px">#</th>
					<th>Nama</th>
					<th>Umur</th>
					<th>Bio</th>
					<th style="width: 40px">Action</th>
				</tr>
			</thead>
			<tbody>
                @forelse ($data as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
						<td>{{$value->bio}}</td>
                        <td style="display:flex">
                            <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td  colspan="4"><div align="center">No data</div></td>
                    </tr>  
                @endforelse              
            </tbody>
		</table>
	</div>
	
@endsection

@push('scripts')
	
@endpush