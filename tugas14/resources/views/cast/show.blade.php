@extends('adminlte.master')

@section('content')
	
	<div class="card-body">		
		<h2>Show Cast {{$cast->id}}</h2>
		<h4>Nama : {{$cast->nama}}</h4>
		<p>Umur : {{$cast->umur}}</p>
		<p>Bio : {{$cast->bio}}</p>
		
		<button type="button" class="btn btn-primary" onclick="document.location.href='/cast'">Back</button>
	</div>
	
@endsection

@push('scripts')
	
@endpush