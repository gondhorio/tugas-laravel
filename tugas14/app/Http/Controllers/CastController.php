<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    function Create(){
		return view('cast.create');
	}
	
	function Store(Request $request){
		//dd($request->all());
		$validatedData = $request->validate([
			'Nama' => 'required|unique:cast|max:255',
			'Umur' => 'required',
			'Bio' => 'required',
		]);
		
		$query = DB::table('cast')->insert([
			"nama" => $request["Nama"],
			"umur" => $request["Umur"],
			"bio" => $request["Bio"]
		]);
		
		return redirect('/cast');
	}
	
	function Index(){
		$data = DB::table('cast')->get();
		
		return view('cast.index', compact('data'));
	}
	
	public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }
	
	public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'Nama' => 'required|unique:cast',
            'Umur' => 'required',
			'Bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["Nama"],
                'umur' => $request["Umur"],
				'bio' => $request["Bio"]
            ]);
        return redirect('/cast');
    }
	public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
