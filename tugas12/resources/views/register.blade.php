<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <form action="/welcome" method="post">
			@csrf
			
			<h1>Buat Account Baru</h1>
			
			<h2>Sign Up Form</h2>
				
			<p>First Name:</p>
			<input name="FirstName" type="text">
				
			<p>Last Name:</p>
			<input name="LastName" type="text">
			
			<p>Gender:</p>	
			<div><label><input name="Gender" type="radio" value="L"> Male</label></div>
			<div><label><input name="Gender" type="radio" value="P"> Female</label></div>
			<div><label><input name="Gender" type="radio" value="-"> Other</label></div>
			
			<p>Nationality:</p>
			<div>
				<select name="Nationality">
					<option value="Indonesia">Indonesia</option>
					<option value="Singapore">Singapore</option>
					<option value="Malaysia">Malaysia</option>
					<option value="Kamboja">Kamboja</option>
				</select>
			</div>
			
			<p>Language Spoken:</p>
			<div><label><input name="Language" type="checkbox" value="Indonesia"> Bahasa Indonesia</label></div>
			<div><label><input name="Language" type="checkbox" value="English"> English</label></div>
			<div><label><input name="Language" type="checkbox" value="Other"> Other</label></div>
			
			<p>Bio:</p>
			<div><textarea name="Bio" cols="auto" rows="6"></textarea></div>
			
			<input name="btnsubmit" type="submit" value="Sign Up">
		</form>

    </body>
</html>
