<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function Register(){
		return view("register");
	}
	
	function Welcome(Request $req){		
		$FirstName = $req["FirstName"];
		$LastName = $req["LastName"];		
		return view('welcome',["FirstName" => $FirstName, "LastName" => $LastName]);
	}
}
