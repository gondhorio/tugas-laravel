<?php
require("animal.php"); 
$sheep = new Animal("shaun");

echo '<h1>Release 0</h1>';
echo 'Akses dari property : <br>';
echo 'Name : '.$sheep->name; // "shaun"
echo '<br>';
echo 'Legs : '.$sheep->legs; // 4
echo '<br>';
echo 'Cold Blooded : '.$sheep->cold_blooded; // "no"
echo '<br>';

echo '<br>';
echo 'Menggunakan Method :';
echo '<br>';
echo 'Name : '.$sheep->get_name(); // "shaun"
echo '<br>';
echo 'Legs : '.$sheep->get_legs(); // 4
echo '<br>';
echo 'Cold Blooded : '.$sheep->get_cold_blooded(); // "no"
echo '<br>';

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo '<h1>Release 1</h1>';
require("Ape.php");
require("Frog.php");

$sungokong = new Ape("kera sakti");
echo '<br>Name : '.$sungokong->name; 
echo '<br>';
echo 'Legs : '.$sungokong->legs; 
echo '<br>';
echo 'Cold Blooded : '.$sungokong->cold_blooded; 
echo '<br>';
echo 'Yell : ';
$sungokong->yell(); 

echo '<br><br>';

$kodok = new Frog("buduk");
echo '<br>Name : '.$kodok->name; 
echo '<br>';
echo 'Legs : '.$kodok->legs;
echo '<br>';
echo 'Cold Blooded : '.$kodok->cold_blooded; 
echo '<br>';
echo 'Jump : ';
$kodok->jump() ; 

?>